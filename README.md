# TCC git hooks

TCC git hooks are a collection of git hook scripts to manage TCC git commits.

## System requirements

  - Python 3.5.0
  - Cygwin (Only required for egit)

## How to install

1. Download and install [Python 3.5.0](https://www.python.org/downloads/release/python-350/)
2. If you are using egit (Eclipse git), please install [Cygwin](https://www.cygwin.com/)
    - Download and execute the [Cygwin installer](https://www.cygwin.com/setup-x86_64.exe)
    - Use default settings for Cygwin setup
    - In **Select Packages**, just install **Devel** and **Shells** packages
    - After installation, add Cygwin bin directory (usually C:\\cygwin64\bin) to path
3. Clone this repository and put all scripts in client directory to .git/hooks under the root directory of each project 

## Usage

* [Command line](#commandline)
* [TortoiseGit](#tortoisegit)
* [Egit (Eclipse git)](#egit)

### <a name="commandline"></a>Command line

Just use the common git command to commit your code.
```sh
$ git commit -a
```

You should be able to see messages that tell you if the commit is valid.

### <a name="tortoisegit"></a>Tortoise git

1. Right click the project directory, select **TortoiseGit** -> **Settings**
2. In the left pane, select **Hook Scripts** and click **Add** button
3. Choose the script you want to bind to Tortoise
    - Hook Type: The hook type of Tortoise is a little difference with original git. Please reference the table below to select the correct type.
    - Working Tree Path: The parent directory path of your hook script
    - Command Line To Execute: The path of the hook script
4. Check **Wait for the script to finish** and save settings
5. Use the common way to commit your code through Tortoise git

**Note!** Tortoise git cannot execute Python script directly, so please select the hook script without **.py** suffix. It will then call the corresponding Python script. 

| Original git hook name | Tortoise hook type | When to use                           |
| ---------------------- | :----------------: | :------------------------------------ |
| pre-commit             | Start Commit Hook  | Run before the commit dialog is shown |
| commit-msg             | Pre-Commit Hook    | Called after the user clicks **OK** in the commit dialog |
| pre-push               | Pre-Push Hook      | Called before actual Git push begins  |
| None                   | Post-Push Hook     | Called after pushing finishes         |

### <a name="egit"></a>Egit

Egit needs the external shell command to execute the hook scripts. So you have to install Cygwin first to make sh.exe and git.exe available.

After installing Cygwin, you can just use the common way to commit your code.
