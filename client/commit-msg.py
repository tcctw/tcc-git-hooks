#! /usr/bin/env python

import sys
import re

message_file = sys.argv[1]
messages = open(message_file, 'r', encoding="utf-8").readlines()
  
# Match the first line with {ticket_number} - {ticket_title} : {abstract}
match = re.match(r"^(?P<ticket_number>\d{5,})\s*-\s*(?P<ticket_title>[^:]+):\s*(?P<abstract>.*)", messages[0])

if match is None:
  print('Your commit message is invlalid!')
  print('Please follow the format in your first line: {ticket_number} - {ticket_title} : {abstract}')
  print('You can also add any optional details after the first line.')
  sys.exit(1)

# Read the rest line as details
details = "";
for line in messages[1:]:
  line = line.strip()
  if not len(line):
    continue
  details += line

print('Your commit message is valid.')
print('Ticket number: ' + match.group('ticket_number').strip())
print('Ticket title: ' + match.group('ticket_title').strip())
print('Abstract: ' + match.group('abstract').strip())
print('Details: ' + details)
sys.exit(0)